<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2 Activity</title>
</head>
<body>
	<h1>Divisibles of Five</h1>
	<?php divisibleByFive(); ?>

	<h1>Array Manipulation</h1>
	<?php $students = []; ?>
	<pre><?php echo var_dump($students) ?></pre>

	<?php array_push($students, 'John Smith'); ?>
	<pre><?= var_dump($students); ?></pre>
	<p><?= count($students) ?></p>

	<?php array_push($students, 'Jane Smith'); ?>
	<pre><?= var_dump($students); ?></pre>
	<p><?= count($students) ?></p>

	<?php array_shift($students); ?>
	<pre><?= var_dump($students); ?></pre>
	<p><?= count($students) ?></p>



</body>
</html>